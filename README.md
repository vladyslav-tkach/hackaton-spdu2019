**Registration**
----
   Registers the user and returns access token.

* **URL**

   /signup/

* **Method:**

   `POST`

*  **URL Params**

   None

* **Data Params**

   ```json
   {
    "password": "12345678",
    "firstName": "John",
    "lastName": "Greg",
    "email": "John.Greg@gmail.com",
    "roles": ["admin"]
  }
  ```

* **Success Response:**

  * **Code:** 201 <br />
    **Content:**
    ```json
    {
      "accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTQsImlhdCI6MTU4MTYyMzYxOSwiZXhwIjoxNTgxNzEwMDE5fQ.6LIe1oG1W-JM9F8hkWh9tpAdyEarPPEswrFTRrZMWM0"
    }
    ```
 
* **Error Response:**

  * **Code:** 400 BAD REQUEST <br />
    **Content:** 
    ```json
    {
      "error": "Registration failed. User with this email is already exists."
    }
    ```

  OR

  * **Code:** 400 BAD REQUEST <br />
    **Content:** 
    ```json
    {
      "error": "Validation error: Password cannot be empty"
    }
    ```
  OR

  * **Code:** 400 BAD REQUEST <br />
    **Content:** 
    ```json
    {
      "error": "Validation error: Password should be at least 6 chars long"
    }
    ```

  OR

  * **Code:** 400 BAD REQUEST <br />
    **Content:** 
    ```json
    {
      "error": "Validation error: First Name cannot be empty"
    }
    ```

  OR

  * **Code:** 400 BAD REQUEST <br />
    **Content:** 
    ```json
    {
      "error": "Validation error: Last Name cannot be empty"
    }
    ```
   
  OR

  * **Code:** 400 BAD REQUEST <br />
    **Content:** 
    ```json
    {
      "error": "Validation error: Not valid email address"
    }
    ```

  OR

  * **Code:** 400 BAD REQUEST <br />
    **Content:** 
    ```json
    {
      "error": "Registration failed. Please provide user roles."
    }
    ```