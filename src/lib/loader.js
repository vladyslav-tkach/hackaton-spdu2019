const path = require('path');
const fs = require('fs');

const upperFirst = (string) => (
  string
    .split('')
    .reduce((result, char, index) => result + ((index ? char : char.toUpperCase())), '')
);

const getKeyName = (string) => (
  string
    .split('.')
    .reduce((result, word, index) => result + (index ? upperFirst(word) : word), '')
);

module.exports = (entities) => {
  if (typeof entities !== 'string') {
    return {};
  }

  const basePath = path.join(__dirname, '..', entities);
  if (!fs.existsSync(basePath)) {
    return {};
  }

  const files = fs
    .readdirSync(basePath)
    .filter((file) => (file.indexOf('.') !== 0) && (file.slice(-3) === '.js'))
    .map((file) => path.join(basePath, file));

  const modules = {};
  if (files && files.length) {
    files.forEach((file) => {
      const key = getKeyName(path.basename(file).replace(/.js$/, ''));
      modules[key] = require(file); // eslint-disable-line import/no-dynamic-require, global-require
    });
  }

  return modules;
};
