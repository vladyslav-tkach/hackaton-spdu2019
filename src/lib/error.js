const { STATUS_CODES } = require('http');

module.exports = (statusCode, message) => {
  const err = message instanceof Error
    ? message
    : new Error(message || STATUS_CODES[statusCode]);

  err.status = statusCode;
  return err;
};
