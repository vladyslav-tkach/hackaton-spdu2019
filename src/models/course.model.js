module.exports = (sequelize, DataTypes) => {
  const Course = sequelize.define('Course', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          args: true,
          msg: 'Course name cannot be empty'
        }
      }
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          args: true,
          msg: 'Course description cannot be empty'
        }
      }
    }
  });

  Course.associate = ({ Student, Task }) => {
    Course.belongsToMany(Student, {
      through: 'StudentCourse',
      foreignKey: 'courseId',
      otherKey: 'studentId'
    });
    Course.hasMany(Task, { foreignKey: 'courseId' });
  };

  return Course;
};
