module.exports = (sequelize, DataTypes) => {
  const StudentCourse = sequelize.define('StudentCourse', {
    studentId: {
      type: DataTypes.INTEGER,
    },
    courseId: {
      type: DataTypes.INTEGER,
    }
  },
  {
    timestamps: false
  });

  StudentCourse.associate = ({ Student, Course }) => {
    StudentCourse.belongsTo(Student, { foreignKey: 'studentId' });
    StudentCourse.belongsTo(Course, { foreignKey: 'courseId' });
  };

  return StudentCourse;
};
