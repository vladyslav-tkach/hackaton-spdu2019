const Sequelize = require('sequelize');
const path = require('path');
const fs = require('fs');
const config = require('../config/db.config');

const basename = path.basename(__filename);

const {
  username,
  password,
  database,
  params
} = config;
const sequelize = new Sequelize(database, username, password, params);
const db = {};

fs
  .readdirSync(__dirname)
  .filter((file) => (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js'))
  .forEach((file) => {
    const model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.Sequelize = Sequelize;
db.sequelize = sequelize;

module.exports = db;
