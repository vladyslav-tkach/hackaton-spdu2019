module.exports = (sequelize, DataTypes) => {
  const Task = sequelize.define('Task', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          args: true,
          msg: 'Task name cannot be empty'
        }
      }
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notEmpty: {
          args: true,
          msg: 'Task description cannot be empty'
        }
      }
    },
    status: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'new',
      validate: {
        notEmpty: {
          args: true,
          msg: 'Task status cannot be empty'
        }
      }
    },
    estimatedTime: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 86400,
      validate: {
        notEmpty: {
          args: true,
          msg: 'Task estimated time cannot be empty'
        }
      }
    },
    timeStart: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: null,
    },
    timeOfWork: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: null,
    },
    deadLine: {
      type: DataTypes.DATE,
      allowNull: false,
    }
  });

  Task.associate = ({ Student, Course }) => {
    Task.belongsTo(Student, {
      onDelete: 'CASCADE',
      foreignKey: {
        name: 'studentId',
        allowNull: false
      }
    });
    Task.belongsTo(Course, {
      onDelete: 'CASCADE',
      foreignKey: {
        name: 'courseId',
        allowNull: false
      }
    });
  };

  return Task;
};
