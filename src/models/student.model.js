const bcrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {
  const Student = sequelize.define('Student', {
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      set(val) {
        if (val && val.toLowerCase) {
          this.setDataValue('email', val.toLowerCase());
        }
      },
      validate: {
        isEmail: {
          args: true,
          msg: 'Not valid email address'
        },
        async isUnique(value) {
          const email = await Student.findOne({
            where: { email: value }
          });
          if (email) {
            throw new Error('Email already exists');
          }
        }
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          args: true,
          msg: 'Password cannot be empty'
        },
        len: {
          args: [6, 128],
          msg: 'Password should be between 6 and 128 characters in length'
        }
      }
    },
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          args: true,
          msg: 'First Name cannot be empty'
        }
      }
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          args: true,
          msg: 'Last Name cannot be empty'
        }
      }
    },
    gitLabToken: {
      type: DataTypes.STRING,
      allowNull: false
    },
    info: {
      type: DataTypes.VIRTUAL,
      get() {
        return {
          id: this.id,
          firstName: this.firstName,
          lastName: this.lastName,
          email: this.email,
          gitLabToken: this.gitLabToken,
          createdAt: this.createdAt,
          updatedAt: this.updatedAt
        };
      }

    }
  },
  {
    defaultScope: {
      attributes: { exclude: ['password'] }
    },
    scopes: {
      withPassword: { attributes: {} }
    }
  });

  const hashPasswordHook = async (user) => {
    if (user.changed('password')) {
      const salt = await bcrypt.genSalt(10);
      const hashedPassword = await bcrypt.hash(user.password, salt);
      user.password = hashedPassword;
    }
  };

  Student.beforeCreate(hashPasswordHook);
  Student.beforeUpdate(hashPasswordHook);

  Student.prototype.checkPassword = async function checkPassword(password) {
    return bcrypt.compare(password, this.password);
  };

  Student.associate = ({
    AuthToken,
    Course,
    StudentCourse,
    Task
  }) => {
    Student.hasMany(AuthToken, { foreignKey: 'studentId' });
    Student.hasMany(Task, { foreignKey: 'studentId' });
    Student.belongsToMany(Course, {
      through: StudentCourse,
      foreignKey: 'studentId',
      otherKey: 'courseId'
    });
  };

  return Student;
};
