module.exports = (sequelize, DataTypes) => {
  const AuthToken = sequelize.define('AuthToken', {
    token: {
      type: DataTypes.STRING,
      allowNull: false
    },
    expireAt: {
      type: DataTypes.DATE,
      allowNull: false
    }
  },
  {
    timestamps: false
  });

  AuthToken.associate = ({ Student }) => {
    AuthToken.belongsTo(Student, {
      onDelete: 'CASCADE',
      foreignKey: {
        name: 'studentId',
        allowNull: false
      }
    });
  };

  return AuthToken;
};
