module.exports = ({
  Student,
  Task,
  Course,
  sequelize
}) => ({
  async getStudents() {
    return Student.findAll();
  },

  async getStudentById(studentId) {
    return Student.findByPk(studentId);
  },

  async getStudentByEmail(email, withPassword = false) {
    if (withPassword) {
      return Student.scope('withPassword')
        .findOne({
          where: { email }
        });
    }

    return Student.findOne({
      where: { email }
    });
  },

  async addStudent(email, firstName, lastName, password, gitLabToken, courses) {
    const student = await Student.create({
      email,
      firstName,
      lastName,
      password,
      gitLabToken
    });
    await this.setStudentCourses(student, courses);
    return student;
  },

  async deleteStudent(studentId) {
    const student = await this.getStudentById(studentId);
    if (student) {
      await student.destroy();
    }
  },

  async updateStudent(student, payload) {
    const studentColumns = Object.keys(student.dataValues);
    Object.entries(payload)
      .forEach(([key, value]) => {
        if (student[key] && studentColumns.indexOf(key) !== 0) {
          student[key] = value;
        }
      });

    return student.save();
  },

  async setStudentCourses(student, courses) {
    await student.setCourses(courses);
  },

  async getRatingByTask({ id }) {
    return Student.findAll({
      attributes: { exclude: ['info'] },
      include: [{
        model: Task,
        where: {
          id,
          status: 'closed'
        },
        attributes: ['timeOfWork']
      }],
      order: [
        [Task, 'timeOfWork', 'DESC']
      ]
    });
  },

  async getRatingByCourse({ id }) {
    return Student.findAll({
      attributes: { exclude: ['info'] },
      include: [
        {
          model: Task,
          where: {
            status: 'closed'
          },
          attributes: [
            [sequelize.fn('SUM', sequelize.col('timeOfWork')), 'timeOfWork']
          ],
          include: [
            {
              model: Course,
              where: {
                id
              },
              attributes: []
            }
          ]
        }
      ],
      group: 'id',
      order: [
        [sequelize.fn('SUM', sequelize.col('timeOfWork')), 'ASC']
      ]
    });
  },
});
