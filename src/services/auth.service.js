const jwt = require('jsonwebtoken');

module.exports = ({ AuthToken, Sequelize: { Op } }) => ({
  generateToken(payload, expiresIn) {
    return new Promise((resolve, reject) => {
      jwt.sign(payload, process.env.JWT_KEY, { expiresIn }, (err, token) => {
        if (err) {
          reject(err);
        }

        resolve(token);
      });
    });
  },

  verifyToken(token) {
    return new Promise((resolve, reject) => {
      jwt.verify(token, process.env.JWT_KEY, (err, decode) => {
        if (err) {
          reject(err);
        }

        resolve(decode);
      });
    });
  },

  async getAuthToken(token) {
    return AuthToken.findOne({
      where: {
        token,
        expireAt: { [Op.gt]: new Date() }
      }
    });
  },

  async addAuthToken(token, studentId, expireAt) {
    await AuthToken.create({ token, studentId, expireAt });
  },

  async createStudentAuthToken(studentId, expiresIn = 86400) {
    const token = await this.generateToken({ id: studentId }, expiresIn);
    const now = new Date();
    const expireAt = new Date(now.getTime() + expiresIn * 1000);
    await this.addAuthToken(token, studentId, expireAt);
    return token;
  },

  async removeAuthToken(token) {
    await token.destroy();
  },

  async removeExpiredAuthTokens(studentId) {
    await AuthToken.destroy({
      where: {
        studentId,
        expireAt: { [Op.lt]: new Date() }
      }
    });
  },

  async removeAllStudentTokens(student) {
    const studentId = student.id;
    await AuthToken.destroy({
      where: { studentId }
    });
  }
});
