module.exports = ({ Course, Sequelize: { Op } }) => ({
  async getCourseById(courseId) {
    return Course.findByPk(courseId);
  },

  async getCoursesByName(courses) {
    return Course.findAll({
      where: {
        name: { [Op.or]: courses },
      },
    });
  }
});
