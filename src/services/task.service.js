module.exports = ({ Task, Sequelize: { Op } }) => ({
  async getTaskById(taskId) {
    return Task.findByPk(taskId);
  },

  async getTasksByStudent(studentId) {
    return Task.findAll({
      where: { studentId }
    });
  },

  async getActiveTasksByStudent(studentId) {
    return Task.findAll({
      where: {
        studentId,
        status: {
          [Op.not]: 'closed'
        }
      }
    });
  },

  async getTasks(data = {}) {
    const { studentId, courseId } = data;
    return Task.findAll({
      where: {
        studentId,
        courseId
      }
    });
  },

  async updateTask(task, payload) {
    const taskColumns = Object.keys(task.dataValues);
    Object.entries(payload)
      .forEach(([key, value]) => {
        if (task[key] !== undefined && taskColumns.indexOf(key) !== 0) {
          task[key] = value;
        }
      });

    return task.save();
  }
});
