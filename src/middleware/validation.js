module.exports = ({ services, db }) => {
  const authService = services.authService(db);
  const studentService = services.studentService(db);

  return {
    async isStudentExist(req, res, next) {
      try {
        const student = await studentService.getStudentByEmail(req.body.email);
        if (student) {
          return res.status(400).json({ message: 'Registration failed. Student with this email is already exists.' });
        }

        return next();
      } catch (err) {
        return next(err);
      }
    },

    async validateRequestBody(req, res, next) {
      if (!req.is('application/json') || !Object.keys(req.body).length) {
        return res.status(400).json({ message: 'Request body is not valid' });
      }

      return next();
    },

    async validateToken(req, res, next) {
      try {
        const token = req.headers.authorization
          .replace('Bearer ', '')
          .trim();

        if (!token) {
          return res.status(403).json({ message: 'No token provided' });
        }

        const authToken = await authService.getAuthToken(token);
        if (!authToken) {
          return res.status(401).json({ message: 'Invalid token' });
        }

        const decoded = await authService.verifyToken(token);
        if (!decoded) {
          await authService.removeAuthToken(authToken);
          return res.status(401).json({ message: 'Invalid token' });
        }

        const student = await authToken.getStudent();
        if (!student) {
          return res.status(404).json({ message: 'Student not found' });
        }

        req.student = student;
        req.token = authToken;
        return next();
      } catch (err) {
        return next(err);
      }
    }
  };
};
