const error = require('../lib/error');

module.exports = (req, res, next) => next(error(404));
