const { STATUS_CODES } = require('http');

module.exports = (err, req, res, next) => {
  if (res.headersSent) {
    return next(err);
  }

  const { name, status } = err;
  const errorName = name && name.toLowerCase ? name.toLowerCase() : '';
  let errorStatus = status || 500;

  if (errorName.indexOf('validation') !== -1) {
    errorStatus = 400;
  }

  if (errorName.indexOf('uniqueconstraint') !== -1) {
    errorStatus = 400;
  }

  const message = err.message || STATUS_CODES[errorStatus];
  return res.status(errorStatus).json({ message });
};
