module.exports = () => ({
  async setHeaders(req, res, next) {
    res.header(
      'Access-Control-Allow-Headers',
      'Authorization, Origin, Content-Type, Accept',
    );
    return next();
  }
});
