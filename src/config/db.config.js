module.exports = {
  username: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: 'checkroo_hack',
  params: {
    host: process.env.DB_HOST,
    dialect: 'mysql',
    logging: true,
    timezone: process.env.TZ,
    pool: {
      max: 5,
      min: 0,
      acquire: 60000,
      idle: 10000
    },
    define: {
      freezeTableName: true
    }
  }
};
