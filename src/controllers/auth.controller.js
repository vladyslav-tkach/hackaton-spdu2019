module.exports = ({ services, db }) => {
  const authService = services.authService(db);
  const studentService = services.studentService(db);
  const courseService = services.courseService(db);

  return {
    async login(req, res, next) {
      const { email, password } = req.body;
      try {
        const student = await studentService.getStudentByEmail(email, true);
        if (!student) {
          return res.status(404)
            .json({ message: 'User not found' });
        }

        const isValidPassword = await student.checkPassword(password);
        if (!isValidPassword) {
          return res.status(401)
            .json({
              accessToken: null,
              error: 'Invalid password'
            });
        }

        await authService.removeExpiredAuthTokens(student.id);
        const token = await authService.createStudentAuthToken(student.id);
        const studentDetails = await student.info;

        return res.status(200)
          .json({
            studentDetails,
            accessToken: token
          });
      } catch (err) {
        return next(err);
      }
    },

    async signup(req, res, next) {
      const {
        email,
        firstName,
        lastName,
        password,
        gitLabToken,
        courses
      } = req.body;
      try {
        const coursesFromDb = await courseService.getCoursesByName(courses);
        const student = await studentService.addStudent(email, firstName, lastName, password, gitLabToken, coursesFromDb);
        const studentDetails = await student.info;
        const token = await authService.createStudentAuthToken(student.id);

        return res.status(201)
          .json({
            studentDetails,
            accessToken: token
          });
      } catch (err) {
        return next(err);
      }
    },

    async getLoggedStudent(req, res, next) {
      const studentId = Number(req.params.id);
      if (Number.isNaN(studentId)) {
        return res.status(400)
          .json({ message: 'Invalid student id' });
      }

      try {
        const student = await studentService.getStudentById(studentId);
        return res.json(student);
      } catch (err) {
        return next(err);
      }
    },

    async logout(req, res, next) {
      const { token } = req;
      try {
        await authService.removeAuthToken(token);
        return res.status(204)
          .send();
      } catch (err) {
        return next(err);
      }
    },

    async logoutAll(req, res, next) {
      const { student } = req;
      try {
        await authService.removeAllStudentTokens(student);
        return res.status(204)
          .send();
      } catch (err) {
        return next(err);
      }
    }
  };
};
