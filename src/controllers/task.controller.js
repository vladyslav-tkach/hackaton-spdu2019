module.exports = ({ services, db }) => {
  const taskService = services.taskService(db);
  const studentService = services.studentService(db);

  return {
    async getTasks(req, res, next) {
      try {
        const tasks = await taskService.getTasks(req.params);
        if (!tasks) {
          return res.status(404)
            .json({ message: 'There are no tasks.' });
        }

        return res.status(200)
          .json(tasks);
      } catch (err) {
        return next(err);
      }
    },

    async getTasksByStudent(req, res, next) {
      try {
        const studentId = Number(req.params.id);
        if (Number.isNaN(studentId)) {
          return res.status(400)
            .json({ message: 'Invalid student id' });
        }

        const student = await studentService.getStudentById(studentId);
        if (!student) {
          return res.status(404)
            .json({ message: `Student with id ${studentId} does not exist.` });
        }

        const { active } = req.query;
        const tasks = active
          ? await taskService.getActiveTasksByStudent(studentId)
          : await student.getTasks();
        if (!tasks) {
          return res.status(404)
            .json({ message: 'There are no tasks.' });
        }

        return res.status(200)
          .json(tasks);
      } catch (err) {
        return next(err);
      }
    },

    async getTask(req, res, next) {
      const taskId = Number(req.params.id);
      if (Number.isNaN(taskId)) {
        return res.status(400)
          .json({ message: 'Invalid task id' });
      }

      try {
        const task = await taskService.getTaskById(taskId);
        if (!task) {
          return res.status(404)
            .json({ message: `Task with id ${taskId} does not exist.` });
        }

        return res.status(200)
          .json(task);
      } catch (err) {
        return next(err);
      }
    },

    async updateTask(req, res, next) {
      const taskId = Number(req.params.id);
      const payload = req.body;
      if (Number.isNaN(taskId)) {
        return res.status(400)
          .json({ message: 'Invalid task id' });
      }

      try {
        const task = await taskService.getTaskById(taskId);
        if (!task) {
          return res.status(404)
            .json({ message: `Task with id ${taskId} does not exist.` });
        }

        await taskService.updateTask(task, payload);
        return res.status(200)
          .json({ message: `Task with id ${taskId} was successfully updated.` });
      } catch (err) {
        return next(err);
      }
    }
  };
};
