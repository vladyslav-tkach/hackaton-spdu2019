module.exports = ({ services, db }) => {
  const studentService = services.studentService(db);

  return {
    async getCoursesByStudent(req, res, next) {
      try {
        const studentId = Number(req.params.id);
        if (Number.isNaN(studentId)) {
          return res.status(400)
            .json({ message: 'Invalid student id' });
        }

        const student = await studentService.getStudentById(studentId);
        if (!student) {
          return res.status(404)
            .json({ message: `Student with id ${studentId} does not exist.` });
        }

        const courses = await student.getCourses();
        if (!courses) {
          return res.status(404)
            .json({ message: 'There are no courses.' });
        }

        return res.status(200)
          .json(courses);
      } catch (err) {
        return next(err);
      }
    }
  };
};
