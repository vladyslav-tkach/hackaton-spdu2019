module.exports = ({ services, db }) => {
  const studentService = services.studentService(db);
  const taskService = services.taskService(db);
  const courseService = services.courseService(db);

  return {
    async getStudents(req, res, next) {
      try {
        const students = await studentService.getStudents();
        if (!students) {
          return res.status(404)
            .json({ message: 'There are no students.' });
        }

        return res.status(200)
          .json(students);
      } catch (err) {
        return next(err);
      }
    },

    async getStudent(req, res, next) {
      const studentId = Number(req.params.id);
      if (Number.isNaN(studentId)) {
        return res.status(400)
          .json({ message: 'Invalid student id' });
      }

      try {
        const student = await studentService.getStudentById(studentId);
        if (!student) {
          return res.status(404)
            .json({ message: `Student with id ${studentId} does not exist.` });
        }

        return res.status(200)
          .json(student);
      } catch (err) {
        return next(err);
      }
    },

    async updateStudent(req, res, next) {
      const studentId = Number(req.params.id);
      const payload = req.body;
      if (Number.isNaN(studentId)) {
        return res.status(400)
          .json({ message: 'Invalid student id' });
      }

      try {
        const student = await studentService.getStudentById(studentId);
        if (!student) {
          return res.status(404)
            .json({ message: `Student with id ${studentId} does not exist.` });
        }

        const updatedStudent = await studentService.updateStudent(student, payload);
        return res.status(200)
          .json(updatedStudent);
      } catch (err) {
        return next(err);
      }
    },

    async deleteStudent(req, res, next) {
      const studentId = Number(req.params.id);
      if (Number.isNaN(studentId)) {
        return res.status(400)
          .json({ message: 'Invalid student id' });
      }

      try {
        if (!studentId) {
          return res.status(404)
            .json({ message: `Student with id ${studentId} does not exist.` });
        }

        await studentService.deleteStudent(studentId);
        return res.status(200)
          .json({ message: 'Student was deleted successfully.' });
      } catch (err) {
        return next(err);
      }
    },

    async getRatingByTask(req, res, next) {
      const taskId = Number(req.params.id);
      if (Number.isNaN(taskId)) {
        return res.status(400)
          .json({ message: 'Invalid task id' });
      }

      try {
        const task = await taskService.getTaskById(taskId);
        if (!task) {
          return res.status(404)
            .json({ message: `Task with id ${taskId} does not exist.` });
        }

        const students = await studentService.getRatingByTask(task);
        if (!students) {
          return res.status(404)
            .json({ message: `There are no students with ${taskId} id.` });
        }

        return res.status(200)
          .json(students);
      } catch (err) {
        return next(err);
      }
    },

    async getRatingByCourse(req, res, next) {
      const courseId = Number(req.params.id);
      if (Number.isNaN(courseId)) {
        return res.status(400)
          .json({ message: 'Invalid course id' });
      }

      try {
        const course = await courseService.getCourseById(courseId);
        if (!course) {
          return res.status(404)
            .json({ message: `Course with id ${courseId} does not exist.` });
        }

        const students = await studentService.getRatingByCourse(course);
        if (!students) {
          return res.status(404)
            .json({ message: `There are no students with ${courseId} id.` });
        }

        return res.status(200)
          .json(students);
      } catch (err) {
        return next(err);
      }
    }
  };
};
