module.exports = ({
  router,
  middleware,
  controllers,
  services,
  db
}) => {
  const routes = router();

  const courseController = controllers.courseController({ services, db });
  const taskController = controllers.taskController({ services, db });
  const studentController = controllers.studentController({ services, db });
  const { getCoursesByStudent } = courseController;
  const { getTasks, getTasksByStudent } = taskController;
  const { getRatingByTask, getRatingByCourse } = studentController;

  const responseMiddleware = middleware.response();
  const { setHeaders } = responseMiddleware;

  const validationMiddleware = middleware.validation({ services, db });
  const { validateToken } = validationMiddleware;

  routes.use(setHeaders);
  routes.use(validateToken);

  routes
    .get('/students/:id/courses', getCoursesByStudent)
    .get('/students/:id/tasks', getTasksByStudent)
    .get('/students/:studentId/courses/:courseId/tasks', getTasks)
    .get('/students/rating/task/:id', getRatingByTask)
    .get('/students/rating/course/:id', getRatingByCourse);

  return routes;
};
