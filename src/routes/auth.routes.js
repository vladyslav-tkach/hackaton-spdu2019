module.exports = ({
  router,
  middleware,
  controllers,
  services,
  db
}) => {
  const routes = router();
  const authController = controllers.authController({ services, db });
  const {
    signup,
    login,
    logout,
    logoutAll
  } = authController;

  const responseMiddleware = middleware.response();
  const { setHeaders } = responseMiddleware;

  const validationMiddleware = middleware.validation({ services, db });
  const {
    validateToken,
    isStudentExist,
    validateRequestBody
  } = validationMiddleware;

  routes.use(setHeaders);

  routes
    .post('/signup', [
      validateRequestBody,
      isStudentExist,
    ], signup)
    .post('/login', validateRequestBody, login)
    .post('/logout', validateToken, logout)
    .post('/logoutall', validateToken, logoutAll);

  return routes;
};
