module.exports = ({
  router,
  middleware,
  controllers,
  services,
  db
}) => {
  const routes = router();

  const taskController = controllers.taskController({ services, db });
  const { getTask, updateTask } = taskController;

  const responseMiddleware = middleware.response();
  const { setHeaders } = responseMiddleware;

  const validationMiddleware = middleware.validation({ services, db });
  const { validateToken } = validationMiddleware;

  routes.use(setHeaders);
  routes.use(validateToken);

  routes
    .get('/tasks/:id', getTask)
    .put('/tasks/:id', updateTask);

  return routes;
};
