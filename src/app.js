require('dotenv')
  .config();
const express = require('express');
const cors = require('cors');
const compression = require('compression');
const load = require('./lib/loader');

const app = express();
const router = express.Router;
const PORT = process.env.PORT || 3005;

// global middleware
app.use(compression());
app.use(cors());
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({
  limit: '50mb',
  extended: false
}));

// routes
const routes = load('routes');

// middleware
const middleware = load('middleware');

// controllers
const controllers = load('controllers');

// services
const services = load('services');

// database and models
const db = require('./models');

Object.keys(routes)
  .forEach((routeName) => {
    app.use('/api', routes[routeName]({
      router,
      middleware,
      controllers,
      services,
      db
    }));
  });

// error handling
const { notFound, error } = middleware;
app.use(notFound);
app.use(error);

db.sequelize.sync()
  .then(() => {
    console.log('Database connected');

    // server
    app.listen(PORT, (err) => {
      if (err) {
        console.error(err);
        return;
      }

      console.log(`Express server running on port ${PORT}`);
    });
  });
